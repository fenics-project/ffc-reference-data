------------------
FFC reference data
------------------

This repository contains data used by the regression tests for the
FEniCS Form Compiler (FFC). The reference data are stored in a
separate repository to minimize the size of the main FFC repository.

This repository is intended to be checked out inside the ffc repository as:

    <ffcdir>/tests/regression/ffc-reference-data/

while the ffc repository contains the file:

    <ffcdir>/tests/regression/ffc-reference-data-id

which should contain a git commit id from the ffc-reference-data repository.
This commit id acts as a link from any ffc commit to any ffc-reference-data commit,
showing which regression data version should be used for which ffc repository version.

Downloading and uploading data from/to this repository is automatically
handled by the scripts:

    <ffcdir>/tests/regression/scripts/download
    <ffcdir>/tests/regression/scripts/upload

which are documented in <ffcdir>/tests/regression/README.rst.

