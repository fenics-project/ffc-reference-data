# Copyright (C) 2005-2007 Anders Logg, 2023 Drew Parsons
#
# This file is part of FFC.
#
# FFC is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FFC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with FFC. If not, see <http://www.gnu.org/licenses/>.
#
# The bilinear form a(u, v) for tensor-weighted Poisson's equation.
#
# Compile this form with FFC: ffc TensorWeightedPoisson.ufl

from ufl_legacy import (Coefficient,dx, FiniteElement, grad, inner,
                 TensorElement, TestFunction, TrialFunction, triangle)

P1 = FiniteElement("Lagrange", triangle, 1)
P0 = TensorElement("Discontinuous Lagrange", triangle, 0, (2, 2))

u = TrialFunction(P1)
v = TestFunction(P1)
f = Coefficient(P1)

C = Coefficient(P0)

a = inner(C*grad(u), grad(v))*dx
