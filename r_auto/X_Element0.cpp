#include "../../ufctest.h"
#include "X_Element0.h"
#include <fstream>

int main(int argc, char * argv[])
{
  const char jsonfilename[] = "X_Element0.json";
  std::ofstream jsonfile(jsonfilename);
  Printer printer(jsonfile);
  printer.begin();


  x_element0_finite_element_0 e0; test_finite_element(e0, 0, printer);

  printer.end();
  return 0;
}
