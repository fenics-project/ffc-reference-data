# Copyright (C) 2007 Anders Logg, 2023 Drew Parsons
#
# This file is part of FFC.
#
# FFC is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FFC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with FFC. If not, see <http://www.gnu.org/licenses/>.
#
# Test form for scalar and vector constants.
#
# Compile this form with FFC: ffc Constant.ufl

from ufl_legacy import (Coefficient, dx, FiniteElement, grad, inner,
                 TestFunction, TrialFunction, triangle, VectorElement)

element = FiniteElement("Lagrange", triangle, 1)

v = TestFunction(element)
u = TrialFunction(element)
f = Coefficient(element)

c = Coefficient(FiniteElement("Real", triangle, 0))
d = Coefficient(VectorElement("Real", triangle, 0))

a = c*inner(grad(u), grad(v))*dx
L = inner(d, grad(v))*dx
