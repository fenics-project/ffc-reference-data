# Copyright (C) 2013 Martin S. Alnaes, 2023 Drew Parsons
#
# This file is part of FFC.
#
# FFC is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FFC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with FFC. If not, see <http://www.gnu.org/licenses/>.
#
# A functional M involving a bunch of cell geometry quantities in ufl.
#
# Compile this form with FFC: ffc CellGeometry.ufl

from ufl_legacy import (avg, CellVolume, Circumradius, Coefficient, ds, dS, dx,
                 FacetArea, FacetNormal, FiniteElement, SpatialCoordinate,
                 TestFunction, TrialFunction, tetrahedron)

cell = tetrahedron

V = FiniteElement("CG", cell, 1)
u = Coefficient(V)

# TODO: Add all geometry for all cell types to this and other demo files, need for regression test.
x = SpatialCoordinate(cell)
n = FacetNormal(cell)
vol = CellVolume(cell)
rad = Circumradius(cell)
area = FacetArea(cell)

M = u*(x[0]*vol*rad)*dx + u*(x[0]*vol*rad*area)*ds # + u*area*avg(n[0]*x[0]*vol*rad)*dS

